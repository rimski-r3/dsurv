#################################################################################################################
#  \file project.py
#  \brief
#
# @ Author Elma Buljubasic, Isidor Rimski
#
#  \notes
#
#  \history 12.02.2020. Initial version
#################################################################################################################
import sys
import json
import datetime
from multiprocessing import Process, Queue
from msg_passing_api import *

#global
LOCAL_PORT_BASE = 6060
localID = -1

# messages
TOKEN = "<M>"
PARENT = "<parent>"
ALREADY = "<already>"

def printP(msg):
    now = datetime.datetime.now()
    print(now.strftime("%Y-%m-%d %H:%M:%S") + " PROCES " + str(localID) + ": " + msg)

def parseArguments(argv):
    _localID = -1
    _rootID = -1
    _treeID = -1
    status = "ERROR"
    if len(argv) == 4:
        try:
            _localID = int(argv[1])
            _rootID = int(argv[2])
            _treeID = int(argv[3])
            status = "OK"
        except:
            printP("INVALID program arguments.")
            return(status, None, None, None)
    else:
        printP("please insert program arguments.")
        return (status, None, None, None)

    return (status, _localID, _rootID, _treeID)


def getProcessorsTree(file, numOfTree):
    try:
        f = open(file, 'r')
    except:
        printP("File not found: " + file)
        return []
    try:
        tree = json.load(f)["ProcessorsTree"]
        f.close()
        tree = tree[int(numOfTree)]
    except:
        printP("Wrong json format...")
        return []

    printP("Success get processors tree")
    return tree


def printResult(p, c, o):
    printP("PARENT: " + str(p))
    printP("CHILDREN: " + str(c))
    printP("OTHER: " + str(o))


def main():
    treeID = -1
    local_full_addr = ("", "")
    global localID
    root_full_addr = ("", "")
    rootID = -1
    neighbors_full_addrs = []
    neighborsID = []

    algoritamFinished = False

    # out
    parent = None
    parent_full_addr = None
    children = []
    children_full_addrs = []
    other = []

    # parse input parameters [1] processorsTree, [2] isInitiator
    (status, localID, rootID, treeID) = parseArguments(sys.argv)
    if(status != "OK"):
        quit()

    # get addresses from json
    processorTree = getProcessorsTree('processorsTree.json', treeID)
    if (processorTree != []):
        printP("TREE INFO: " + str(processorTree["info"]))
        local_full_addr = (processorTree["processors"][localID]["address"], LOCAL_PORT_BASE + localID)

        root_full_addr = (processorTree["processors"][rootID]["address"], LOCAL_PORT_BASE + rootID)

        for id in processorTree["processors"][localID]["neighbors"]:
            neighborsID.append(id)
            neighbors_full_addrs.append((processorTree["processors"][int(id)]["address"], LOCAL_PORT_BASE + id))
        printP("NEIGHBORS: " + str(neighborsID))
    else:
        quit()

    #proces project
    # Create queue for messages from the local server
    queue = Queue()

    # Create and start server process
    server = Process(target=server_fun, args=(local_full_addr, queue))
    server.start()

    shouldRun = True
    while shouldRun:

        #if procesor are initiator
        if(localID == rootID and parent == None):
            #send TOKEN to all neighbors
            printP("send TOKEN to: " + str(neighborsID))
            broadcastMsg(neighbors_full_addrs, TOKEN + str(localID))
            parent = rootID
            parent_full_addr = root_full_addr
            printP("STORED PARENT: " + str(parent))

        #receive token
        msg = rcvMsg(queue)
        printP("receive " + str(msg))




        if(msg.startswith(TOKEN)):
            if algoritamFinished == True:
                break

            #get id and validate msg
            try:
                sender_id = int(msg[len(TOKEN):])
            except:
                printP("receive wrong message")
                continue
            printP("receive TOKEN from " + str(sender_id))
            sender_addr = (processorTree["processors"][sender_id]["address"], LOCAL_PORT_BASE + sender_id)
            if(parent == None):
                #store him as parent
                parent = sender_id
                parent_full_addr = sender_addr
                printP("STORED PARENT: " + str(parent))
                printP("send PARENT to " + str(sender_id))
                #send to him PARENT
                try:
                    sendMsg(sender_addr, PARENT + str(localID))
                except:
                    printP("NOT ALIVE " + str(sender_id))

                #send to others neighbors
                neighbors_full_addrs_without_sender = [x for x in neighbors_full_addrs if x != sender_addr]
                neighborsID__without_sender = [x for x in neighborsID if x != sender_id]

                if neighbors_full_addrs_without_sender:
                    printP("send TOKEN to " + str(neighborsID__without_sender))
                    for i in range(neighbors_full_addrs_without_sender.__len__()):
                        try:
                            sendMsg(neighbors_full_addrs_without_sender[i], TOKEN + str(localID))
                        except:
                            printP("NOT ALIVE " + str(neighborsID__without_sender[i]))

            else:
                #send him alredy heave parent
                printP("send ALREADY to " + str(sender_id))
                try:
                    sendMsg(sender_addr, ALREADY + str(localID))
                except:
                    printP("NOT ALIVE " + str(sender_id))





        if (msg.startswith(PARENT)):
            if algoritamFinished == True:
                break
            # get id and validate msg
            try:
                sender_id = int(msg[len(PARENT):])
            except:
                printP("receive wrong message")
                continue
            printP("receive PARENT from " + str(sender_id))
            sender_addr = (processorTree["processors"][sender_id]["address"], LOCAL_PORT_BASE + sender_id)

            children.append(sender_id)
            children_full_addrs.append(sender_addr)
            printP("store to CHILDREN: " + str(sender_id))





        if (msg.startswith(ALREADY)):
            if algoritamFinished == True:
                break
            # get id and validate msg
            try:
                sender_id = int(msg[len(ALREADY):])
            except:
                printP("receive wrong message")
                continue
                printP("receive ALREADY from " + str(sender_id))

            other.append(sender_id)
            printP("store to OTHER: " + str(sender_id))
            sender_addr = (processorTree["processors"][sender_id]["address"], LOCAL_PORT_BASE + sender_id)



        
        # if all was procesed terminate then
        if(algoritamFinished == False):
            finished = True
            for id in neighborsID:
                if (id not in children) and (id not in other) and id != parent:
                    finished = False
                    break

            if finished == True:
                algoritamFinished = True
                printP("algoritam finished")
                printResult(parent, children, other)
                break

    # Join with server process
    server.join()

    # Delete queue and server
    del queue
    del server

if __name__ == '__main__':
    main()
