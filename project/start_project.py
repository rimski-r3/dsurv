#################################################################################################################
#  \file start_project.py
#  \brief
#
# @ Author Elma Buljubasic, Isidor Rimski
#
#  \notes
#
#  \history 12.02.2020. Initial version
#################################################################################################################

import sys
import subprocess
from subprocess import Popen, PIPE
import random
import json
import time

def getProcessorsTree(file, numOfTree):
    try:
        f = open(file, 'r')
    except:
        print("TEST: File not found: " + file)
        return []
    try:
        tree = json.load(f)["ProcessorsTree"]
        f.close()
        tree = tree[int(numOfTree)]
    except:
        print("TEST: Wrong json format...")
        return []

    print("TEST: Succses get processors tree")
    return tree


def main():
    procesors = []

    if (sys.argv.__len__() != 3):
        print("TEST: insert id of tree from json file")
        exit()
    try:
        root = int(sys.argv[1])
        treeID = int(sys.argv[2])
    except:
        print("TEST: INVALID program arguments.")
        exit()

    # get addresses from json
    processorTree = getProcessorsTree('processorsTree.json', treeID)
    if (processorTree == []):
        print("TEST: Error reading a json")
        exit()

    print("TEST: tree INFO: ", processorTree["info"])
    len = (processorTree["processors"].__len__())
    print("TEST: ROOT is ", root)

    list_of_procesors_without_root = [x for x in range(len) if x != root]
    #randomize list
    random.shuffle(list_of_procesors_without_root)
    print(list_of_procesors_without_root)

    #commands
    commands = ["python.exe project.py " + str(procesor) + " " + str(root) + " " + str(treeID) for procesor in list_of_procesors_without_root]
    root_command = "python.exe project.py " + " " + str(root) + " " + str(root) + " " + str(treeID)
    procs = []

    for cmd in commands:
        print("TEST: ", cmd)
        procs.append(Popen(cmd, shell=True))
        time.sleep(0.5)

    print()
    print()
    print("TEST: STARTED all receive processors")
    print()
    print()

    #sleep 2s
    time.sleep(2)
    print("TEST: ", root_command)
    commands.append(root_command)
    procs.append(Popen(root_command, shell=True))

    print(" ")
    print(" ")
    print("TEST: STARTED root processor")
    print(" ")
    print(" ")

    for i in range(procs.__len__()):
        print("TEST: wait proces", commands[i])
        procs[i].wait()

    print(" TEST: TERMINATED")

if __name__ == '__main__':
    main()