import sys
from multiprocessing import Process, Queue
from multiprocessing.connection import Client, Listener
from array import array

def server_fun(local_port, queue):
    # Set the address of the local node's server
    local_server_address = ('localhost', local_port)
    
    # Send fixed message
    with Listener(local_server_address, authkey=b'Lets work together') as listener:
        
        while True:
            with listener.accept() as conn:
                #print('connection accepted from', listener.last_accepted)
                msg = conn.recv()
                #print(msg)
                
                # Forward msg to local node's process
                queue.put(msg)
                
                # Exit if msg is 'exit'
                if msg == 'exit':
                    break

def sendMsg(remote_server_address, msg):
    with Client(remote_server_address, authkey=b'Lets work together') as conn:
        conn.send(msg)

def rcvMsg(queue):
    return queue.get()

def main():
    # Parse command line arguments
    if len(sys.argv) != 3:
        print('Program usage: example2_node_to_node local_port remote_port, e.g.   example2_node_to_node 6000 6001')
        print('Note: This program expects that its peer instance swaps ports, e.g. example2_node_to_node 6001 6000')
        exit()
    
    # Set ports
    local_port =  int( sys.argv[1] )
    remote_port = int( sys.argv[2] )
    
    # Create queue for messages from the local server
    queue = Queue()
    
    # Create and start server process
    server = Process(target=server_fun, args=(local_port,queue))
    server.start()
    
    # Set the address of the peer node's server
    remote_server_address = ('localhost', remote_port)
    
    # Send a message to the peer node and receive message from the peer node.
    # To exit send message: exit.
    print('Send a message to the peer node and receive message from the peer node.')
    print('To exit send message: exit.')
    
    while True:
        # Input message
        msg = input('Enter message: ')
        #print('Message sent: %s \n' % (msg))
        
        # Send message to peer node's server
        sendMsg(remote_server_address, msg)
        
        # Get message from local node's server
        msg2 = rcvMsg(queue)
        print('Message received: %s \n' % (msg2))
        
        if msg == 'exit':
            break
    
    # Join with server process
    server.join()
    
    # Delete queue and server
    del queue
    del server

if __name__ == '__main__':
    main()

