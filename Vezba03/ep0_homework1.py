from multiprocessing import Process, Queue

def childProcFun(q, id, x):
    q.put( (id, x, x*x) )

if __name__ == '__main__':
    no_processes = 3
    q = Queue()
    
    pl = [Process(target=childProcFun, args=(q, id, id+2,)) for id in range(no_processes)]
    
    for p in pl:
        p.start()
    
    for i in range(no_processes):
        print(q.get())
    
    for p in pl:
        p.join()
        